SECRET_SED = sed -e s,@GITHUB_TOKEN\@,${GITHUB_TOKEN}, \
	         -e s,@TRAVIS_TOKEN\@,${TRAVIS_TOKEN}, \
	         -e s,@GITHUB_APP_ID\@,${GITHUB_APP_ID}, \
	         -e s,@GITHUB_APP_SECRET\@,${GITHUB_APP_SECRET},

secret.yml: secret.yml.in Makefile
	test -n "${GITHUB_TOKEN}" && test -n "${TRAVIS_TOKEN}"
	$(SECRET_SED) < $< > $@.new && mv $@.new $@

cfg.toml: cfg.toml.in Makefile
	test -n "${GITHUB_TOKEN}" && test -n "${TRAVIS_TOKEN}"
	$(SECRET_SED) < $< > $@.new && mv $@.new $@

all:
	sudo docker build -t cgwalters/homu .
