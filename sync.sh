#!/bin/bash
set -xeuo pipefail
test -f cfg-atomic.toml && rsync -av --delete -e ssh ./ escher.verbum.org:homu-docker/
