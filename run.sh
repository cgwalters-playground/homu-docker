#!/bin/sh
set -euo pipefail
cfg=/home/cfg.toml
sed -e s,@GITHUB_TOKEN\@,$(cat /etc/homu-secrets/github-token), \
    -e s,@TRAVIS_TOKEN\@,$(cat /etc/homu-secrets/travis-token), \
    < ${cfg}.in > ${cfg}.new && mv ${cfg}.new ${cfg}
exec /usr/bin/homu
