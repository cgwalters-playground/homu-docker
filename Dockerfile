FROM fedora
RUN yum -y install git python-pip findutils && yum clean all && \
    mkdir /app && cd /app && git clone -b projectatomic-homu-rollup https://github.com/cgwalters/homu.git && pip3 install ./homu && \
    find /usr/lib/python* -name git_helper.py -exec chmod a+x {} ';' && \
    groupadd -g 5000 homu && useradd -u 5000 -g homu homu
USER homu
WORKDIR /homudata
EXPOSE 54856
CMD ["/usr/bin/homu", "-v"]
