docker pull centos
docker create -v /homudata --name homudata centos
docker run --rm -ti --volumes-from homudata centos chown -R 5000 /homudata
docker cp cfg.toml homudata:/homudata

docker build --no-cache -t cgwalters/homu . 
docker rm -f homu ; docker run -p 54856:54856 -d --volumes-from homudata --name homu cgwalters/homu
